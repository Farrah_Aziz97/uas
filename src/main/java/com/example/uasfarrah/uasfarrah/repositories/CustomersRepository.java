package com.example.uasfarrah.uasfarrah.repositories;

import com.example.uasfarrah.uasfarrah.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomersRepository extends JpaRepository<Customer, Integer> {
}
