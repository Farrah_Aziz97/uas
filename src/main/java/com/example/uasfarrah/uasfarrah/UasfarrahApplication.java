package com.example.uasfarrah.uasfarrah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UasfarrahApplication {

    public static void main(String[] args) {
        SpringApplication.run(UasfarrahApplication.class, args);
    }

}

